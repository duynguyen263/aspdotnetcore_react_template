import React, { Component } from 'react';
import './App.scss';
import Alert from './alert';
import FormRegister from './form-register';
import FormSubscription from './form-subscription';

class App extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      display: false
    }
  }

  toggleAlert = (val) => {
    this.setState({
      display: !!val
    })
  }

  alertCallback = () => {
    console.log('ok')
  }

  formRegisterCallback = (data) => {
    console.log(data)
    this.toggleAlert(true)
  }

  render() {
    return (
      <div className="App">
        {
          this.state.display &&
          <Alert display={this.state.display}
            close={() => { this.toggleAlert(false)}}
            callback={this.alertCallback} />
        }
        {/* <FormRegister callback={this.formRegisterCallback} /> */}
        <FormSubscription />
      </div>
    );
  }
}

export default App;
