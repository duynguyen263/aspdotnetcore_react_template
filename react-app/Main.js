// import Home from './src/App.js'

// export default Home

import React, { Component } from 'react';
import { render } from 'react-dom'

class Main extends Component {
    state = {
        text: 'test'
    }
    onChange = (val) => {
        this.setState({ text: val })
    }
    render() {
        return (
            <div>
                <h1>okokok</h1>
                <span className="edit"
                    
                    onChange={e => { this.onChange(e.target.value) }}
                    contentEditable={true}
                    onFocus={(event) => console.log(event)}
                    onKeyUp={(e) => {
                        const value = this.refText.innerText
                        console.log(e.target.selectionStart)
                        if (value != this.state.text) {
                            this.onChange(this.refText.innerText)
                        }
                    }}
                    onSelect={e => { console.log(window.getSelection().toString())}}
                    suppressContentEditableWarning={true}
                >
                <span ref={instance => this.refText = instance} dangerouslySetInnerHTML={ {__html: this.state.text } }></span>
                </span>
            </div>
        );
    }
}

render(<Main />, document.getElementById('app'))

if (module.hot) {
    module.hot.accept()
}

export default null;