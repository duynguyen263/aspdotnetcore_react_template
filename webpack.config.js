const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
	.BundleAnalyzerPlugin;
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = env => {
	const isDevBuild = !(env && env.prod);
	const SharedConfig = () => ({
		output: {
			filename: 'client.js',
			chunkFilename: '[name].js',
			path: path.resolve(__dirname, './wwwroot/dist'),
			publicPath: '/dist/'
		},
		module: {
			rules: [
				{
					test: /\.(js|jsx)$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader'
					}
				},
				{
					test: /\.(jpg|svg|jpeg|gif|png)$/,
					exclude: /node_modules/,
					loader: 'file-loader',
					options: {
						fallback: 'file-loader',
						limit: 1024,
						name(file) {
							return 'images/[path][name].[ext]';
						}
					}
				},
				{
					test: /\.(eot|ttf|woff|woff2)$/,
					loader: 'file-loader?name=fonts/[name].[ext]'
				}
			]
		},
		plugins: [
			new webpack.ProvidePlugin({
				"React": "react",
			}),
			// new CheckerPlugin(),
			new CleanWebpackPlugin([`dist`], {
				root: path.join(__dirname, 'wwwroot'),
				//exclude: ['fonts', 'images'],
				verbose: true,
				dry: false
			})
		],
		resolve: {
			extensions: ['.js', '.jsx']
		}
	});
	const devConfig = () => ({
		mode: 'development',
		entry: {
			main: './react-app/index.js'
		},
		devtool: 'source-map',
		watch: true,
		plugins: [
			new UglifyJsPlugin({
				sourceMap: true,
				cache: true,
				extractComments: false
			}),
			// new BundleAnalyzerPlugin({ analyzerPort: 9999 }),
			new MiniCssExtractPlugin({
				// Options similar to the same options in webpackOptions.output
				// both options are optional
				filename: '[name].css',
				chunkFilename: '[id].css',
			})
		],
		module: {
			rules: [
				{
					test: /\.(sa|sc|c)ss$/,
					use: [
						'style-loader',
						'css-loader',
						'postcss-loader',
						'sass-loader',
					],
				}
			]
		}
	});
	const prodConfig = () => ({
		mode: 'production',
		entry: {
			main: './react-app/index.js'
		},
		devtool: 'source-map',
		plugins: [
			new UglifyJsPlugin({
			    sourceMap: false
			}),
			new BundleAnalyzerPlugin({ analyzerPort: 9999 }),
			new MiniCssExtractPlugin({
				// Options similar to the same options in webpackOptions.output
				// both options are optional
				filename: '[name].[hash].css',
				chunkFilename: '[id].[hash].css'
			})
		],
		module: {
			rules: [
				{
					test: /\.(sa|sc|c)ss$/,
					use: [
						MiniCssExtractPlugin.loader,
						'css-loader',
						'postcss-loader',
						'sass-loader',
					]
				}
			]
		}
	});
	return merge(isDevBuild ? devConfig() : prodConfig(), SharedConfig());
};
